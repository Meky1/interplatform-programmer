# Set variables
$FolderPath = "core"

# Loop 1 - Iterate through each subfolder of $FolderPath
Get-ChildItem -Path $FolderPath -File -Recurse |
    Where-Object { $_.Extension -eq ".c" -or $_.Extension -eq ".h" } |
        ForEach-Object {
            $path = Resolve-Path -relative $_.FullName
            Write-Output "clang-format $path"
            clang-format -i $path
}
