/*****************************************************************************
 * @file hexParser.c
 *
 * @brief Source file for the HEX parser
 *
 * @author Marcin Czarnik
 * @date 1.06.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "middleware/hexParser/hexParser.h"
#include "driver/assertDebug/debug.h"
#include "driver/flashDriver/flashDriver.h"
#include "driver/qspiFlashDriver/qspiFlashDriver.h"
#include "external/mx25l512/mx25l512.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define LENGTH_OF_LINE (46U)
#define ONE_BYTE (1U)
#define LENGTH_OF_ONE_BYTE (2U)
#define LENGTH_OF_TWO_BYTES (4U)
#define NUMBER_OF_BYTES_IN_WORD (4U)

#define NUMBER_OF_BYTES_POS (1U)
#define ADDRESS_POS (3U)
#define RECORD_POS (7U)
#define DATA_POS (9U)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

struct {
    uint8_t numberOfBytes;
    uint16_t address;
    uint8_t recordType;
    uint8_t data[16];
    uint8_t crc;

} hexFrame;

static char sLineOfData[LENGTH_OF_LINE] = { 0 };

static bool sIsEndOfHexFile = false;

static uint32_t sEndOfBinFile = 0;

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/
/**
 * @brief   This function saves the parsed data to the 0x0200000 address in order to create a binary copy
 * @return  true if success, otherwise false
 */
static bool SaveFileDataToExternalMemory(void);

/**
 * @brief   This function reads saved hex data (0x000000 address) and parses them into a structure
 * @return  true if success, otherwise false
 */
static bool ReadLineOfDataFromExternalMemory(void);

/**
 * @brief   This function saves the parsed data to the beginning of the external memory
 * @return  true if success, otherwise false
 */
static bool MoveToBeginningOfMemory(void);

/**
 * @brief   This function parses the string fragment and creates a 8bit value
 * @return  true if success, otherwise false
 */
static uint8_t TurnStringFragmentIntoUint8(uint8_t courserPointer);

/**
 * @brief   This function parses the string fragment and creates a 16bit value
 * @return  true if success, otherwise false
 */
static uint16_t TurnStringFragmentIntoUint16(uint8_t courserPointer);

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

bool HexParserProcess(void)
{
    while (!sIsEndOfHexFile) {
        if (!ReadLineOfDataFromExternalMemory()) {
            LOG_INFO("Failed to read line!\n\r");
            return false;
        }

        if (!SaveFileDataToExternalMemory()) {
            LOG_INFO("Failed to flash with given hex!\n\r");
            return false;
        }
        memset(&hexFrame, 0, sizeof(hexFrame));
    }

    if (!MoveToBeginningOfMemory()) {
        LOG_INFO("Failed to move memory to beginning!\n\r");
        return false;
    }

    return true;
}

uint32_t HexParserGetEndOfBinSection(void)
{
    return sEndOfBinFile;
}


/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static bool SaveFileDataToExternalMemory(void)
{
    static uint32_t externalFlashAddressToSave = MX25L512_FLASH_HEX_RESERVED;

    uint8_t numberOf32BitWords = (uint8_t)hexFrame.numberOfBytes / NUMBER_OF_BYTES_IN_WORD;

    for (uint8_t i = 0; i < numberOf32BitWords; i++) {
        uint32_t dataToBeFlashed = 0;
        dataToBeFlashed += hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i] << 24;
        dataToBeFlashed += hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 1] << 16;
        dataToBeFlashed += hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 2] << 8;
        dataToBeFlashed += hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 3];

        if (externalFlashAddressToSave % MX25L512_SUBSECTOR_SIZE == 0) {
            if (QspiFlashEraseBlock(externalFlashAddressToSave) != QSPI_OK) {
                return false;
            }
        }

        if (QspiFlashWrite((uint8_t *)&dataToBeFlashed, externalFlashAddressToSave,
                           NUMBER_OF_BYTES_IN_WORD) != QSPI_OK) {
            LOG_INFO("Failed to save data in the flash!\n\r");
            return false;
        }

        externalFlashAddressToSave += NUMBER_OF_BYTES_IN_WORD;
    }

    sEndOfBinFile = externalFlashAddressToSave;

    return true;
}

static bool ReadLineOfDataFromExternalMemory(void)
{
    char courser = ' ';
    uint8_t loopCounter = 0;
    static uint32_t externalFlashAddressToRead = MX25L512_START_ADDRESS;

    while (courser != '\n') {
        if (QspiFlashRead((uint8_t *)&courser, externalFlashAddressToRead, ONE_BYTE) != QSPI_OK) {
            LOG_INFO("Failed to read data from external flash!\n\r");
            return false;
        }
        if (loopCounter >= (LENGTH_OF_LINE - 1)) {
            return false;
        }

        sLineOfData[loopCounter] = courser;
        loopCounter++;

        externalFlashAddressToRead += ONE_BYTE;
    }

    hexFrame.numberOfBytes = TurnStringFragmentIntoUint8(NUMBER_OF_BYTES_POS);
    hexFrame.address = TurnStringFragmentIntoUint16(ADDRESS_POS);
    hexFrame.recordType = TurnStringFragmentIntoUint8(RECORD_POS);
    hexFrame.crc = TurnStringFragmentIntoUint8(DATA_POS + LENGTH_OF_ONE_BYTE * hexFrame.numberOfBytes);

    if (hexFrame.recordType == 0x00) {
        for (uint8_t i = 0; i < hexFrame.numberOfBytes; i++) {
            hexFrame.data[i] = TurnStringFragmentIntoUint8(DATA_POS + LENGTH_OF_ONE_BYTE * i);
        }

        uint8_t numberOf32BitWords = (uint8_t)hexFrame.numberOfBytes / NUMBER_OF_BYTES_IN_WORD;
        uint8_t temporaryValueHolder = 0;

        // swapping into little endian [3<->0, 2<->1]
        for (uint8_t i = 0; i < numberOf32BitWords; i++) {

            temporaryValueHolder = hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i];

            hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i] = hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 3];
            hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 3] = temporaryValueHolder;

            temporaryValueHolder = hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 1];

            hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 1] = hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 2];
            hexFrame.data[NUMBER_OF_BYTES_IN_WORD * i + 2] = temporaryValueHolder;
        }
    }

    else if (hexFrame.recordType == 0x01) {
        sIsEndOfHexFile = true;
    }

    return true;
}

static bool MoveToBeginningOfMemory(void)
{
    uint32_t data = 0;
    uint32_t numberOfLoopsNeeded = (sEndOfBinFile - MX25L512_FLASH_HEX_RESERVED) / 4;

    static uint32_t externalFlashAddressToRead = MX25L512_FLASH_HEX_RESERVED;
    static uint32_t externalFlashAddressToSave = MX25L512_START_ADDRESS;

    for (uint32_t i = 0; i < numberOfLoopsNeeded; i++) {

        if (externalFlashAddressToSave % MX25L512_SUBSECTOR_SIZE == 0) {
            if (QspiFlashEraseBlock(externalFlashAddressToSave) != QSPI_OK) {
                return false;
            }
        }

        if (QspiFlashRead((uint8_t *)&data, externalFlashAddressToRead, NUMBER_OF_BYTES_IN_WORD) != QSPI_OK) {
            return false;
        }

        if (QspiFlashWrite((uint8_t *)&data, externalFlashAddressToSave, NUMBER_OF_BYTES_IN_WORD) != QSPI_OK) {
            return false;
        }
        externalFlashAddressToRead += NUMBER_OF_BYTES_IN_WORD;
        externalFlashAddressToSave += NUMBER_OF_BYTES_IN_WORD;
    }
    sEndOfBinFile = externalFlashAddressToSave;
    return true;
}

static uint8_t TurnStringFragmentIntoUint8(uint8_t courserPointer)
{
    uint8_t data = 0;
    char temporaryString[LENGTH_OF_ONE_BYTE + 1] = { 0 };
    strncpy(temporaryString, sLineOfData + courserPointer, LENGTH_OF_ONE_BYTE);
    temporaryString[LENGTH_OF_ONE_BYTE] = '\0';

    data = (uint8_t)strtoul(temporaryString, NULL, 16);  // number base 16

    return data;
}

static uint16_t TurnStringFragmentIntoUint16(uint8_t courserPointer)
{
    uint16_t data = 0;
    char temporaryString[LENGTH_OF_TWO_BYTES + 1] = { 0 };
    strncpy(temporaryString, sLineOfData + courserPointer, LENGTH_OF_TWO_BYTES);
    temporaryString[LENGTH_OF_TWO_BYTES] = '\0';

    data = (uint16_t)strtoul(temporaryString, NULL, 16);  // number base 16

    return data;
}
