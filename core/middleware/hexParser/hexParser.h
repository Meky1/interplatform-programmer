/*****************************************************************************
 * @file hexParser.h
 *
 * @brief Header file for the HEX parser
 *
 * @author Marcin Czarnik
 * @date 1.06.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/**
 * @brief   Process the given hex data into binary data
 * @return	True if success, otherwise false
 */
bool HexParserProcess(void);

/**
 * @brief   Receive the end address of the newly created binary section
 * @return	The end address
 */
uint32_t HexParserGetEndOfBinSection(void);
