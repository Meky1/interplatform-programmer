/*****************************************************************************
 * @file xmodem.h
 *
 * @brief Header file for the xmodem protocol
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/*
 * @brief Handler of the xmodem protocol
 */
void xmodemHandler(void);
