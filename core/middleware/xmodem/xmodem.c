/*****************************************************************************
 * @file xmodem.c
 *
 * @brief Source file for the xmodem protocol
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "middleware/xmodem/xmodem.h"
#include "driver/assertDebug/debug.h"
#include "driver/flashDriver/flashDriver.h"
#include "driver/mcuDriver/mcuDriver.h"
#include "driver/qspiFlashDriver/qspiFlashDriver.h"
#include "driver/uartDriver/uartDriver.h"
#include "external/mx25l512/mx25l512.h"
#include "middleware/hexParser/hexParser.h"
#include "middleware/inAppProg/inAppProg.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

/* Xmodem (128 bytes) packet format
 * Byte  0:       Header
 * Byte  1:       Packet number
 * Byte  2:       Packet number complement
 * Bytes 3-130:   Data
 * Bytes 131-132: CRC
 */

/* Xmodem (1024 bytes) packet format
 * Byte  0:         Header
 * Byte  1:         Packet number
 * Byte  2:         Packet number complement
 * Bytes 3-1026:    Data
 * Bytes 1027-1028: CRC
 */

/* Maximum allowed errors (user defined). */
#define XMODEM_MAX_ERRORS ((uint8_t)3u)
/* Sizes of the packets. */
#define XMODEM_PACKET_NUMBER_SIZE ((uint16_t)2u)
#define XMODEM_PACKET_128_SIZE ((uint16_t)128u)
#define XMODEM_PACKET_1024_SIZE ((uint16_t)1024u)
#define XMODEM_PACKET_CRC_SIZE ((uint16_t)2u)

/* Indexes inside packets. */
#define XMODEM_PACKET_NUMBER_INDEX ((uint16_t)0u)
#define XMODEM_PACKET_NUMBER_COMPLEMENT_INDEX ((uint16_t)1u)
#define XMODEM_PACKET_CRC_HIGH_INDEX ((uint16_t)0u)
#define XMODEM_PACKET_CRC_LOW_INDEX ((uint16_t)1u)

#define XMODEM_SOH ((uint8_t)0x01u) /**< Start Of Header (128 bytes). */
#define XMODEM_STX ((uint8_t)0x02u) /**< Start Of Header (1024 bytes). */
#define XMODEM_EOT ((uint8_t)0x04u) /**< End Of Transmission. */
#define XMODEM_ACK ((uint8_t)0x06u) /**< Acknowledge. */
#define XMODEM_NAK ((uint8_t)0x15u) /**< Not Acknowledge. */
#define XMODEM_CAN ((uint8_t)0x18u) /**< Cancel. */
#define XMODEM_C ((uint8_t)0x43u)   /**< ASCII "C" to notify the host we want to use CRC16. */

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

/* Status report for the functions. */
typedef enum {
    XMODEM_OK = 0x00u,           /**< The action was successful. */
    XMODEM_ERROR_CRC = 0x01u,    /**< CRC calculation error. */
    XMODEM_ERROR_NUMBER = 0x02u, /**< Packet number mismatch error. */
    XMODEM_ERROR_UART = 0x04u,   /**< UART communication error. */
    XMODEM_ERROR_FLASH = 0x08u,  /**< Flash related error. */
    XMODEM_ERROR = 0xFFu         /**< Generic error. */

} xModemStatus_t;

static uint8_t sPacketNumber = 1u;        /**< Packet number counter. */
static bool sFirstPacketReceived = false; /**< First packet or not. */
static uint32_t sEndOfExternalFlashFile = 0;
static fileFormat_t sFileType = 0;

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/*
 * @brief This function is the base of the Xmodem protocol.
 * 		  When we receive a header from UART, it decides what action it shall take.
 */
static bool xmodemReceive(void);

/**
 * @brief   Calculates the CRC-16 for the input package.
 * @param   *data:  Array of the data which we want to calculate.
 * @param   length: Size of the data, either 128 or 1024 bytes.
 * @return  status: The calculated CRC.
 */
static uint16_t CalculateCRC(uint8_t *data, uint16_t length);

/**
 * @brief   This function handles the data packet we get from the xmodem protocol.
 * @param   header: SOH or STX.
 * @return  status: Report about the packet.
 */
static xModemStatus_t HandlePacket(uint8_t size);

/**
 * @brief   Handles the xmodem error.
 *          Raises the error counter, then if the number of the errors reached critical, do a graceful abort, otherwise send a NAK.
 * @param   *error_number:    Number of current errors (passed as a pointer).
 * @param   max_error_number: Maximal allowed number of errors.
 * @return  status: X_ERROR in case of too many errors, X_OK otherwise.
 */
static xModemStatus_t ErrorHandler(uint8_t *errorNumber, uint8_t maxErrorNumber);

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

void xmodemHandler(void)
{
    sFileType = inAppProgReceiveFileType();

    if (!xmodemReceive()) {
        LOG_INFO("XMODEM failed! Jumping back to written program..\n\r");
        // flashEraseApplication();
        // McuDriverSystemReset();
        flashJumpToApp();
    }

    switch (sFileType) {
    case HEX_FILE:
        if (!HexParserProcess()) {
            LOG_INFO("Hex parser has failed!\n\r");
        }
        sEndOfExternalFlashFile = HexParserGetEndOfBinSection();
        break;

    case BIN_FILE:
        break;
    case NO_FILE:
        LOG_INFO("Failed to receive data format!\n\r");
        return;
    default:
        break;
    }

    // if sFileType is different than hex, we assume it is a binary..

    if (inAppProgFlashWithUserApp(sEndOfExternalFlashFile) != false) {
        LOG_INFO("Jumping to user application..\n\r");
        flashJumpToApp();
    }
}


/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static bool xmodemReceive(void)
{
    volatile xModemStatus_t status = XMODEM_OK;
    uint8_t errorNumber = 0u;

    sFirstPacketReceived = false;
    sPacketNumber = 1u;

    /* Loop until there isn't any error (or until we jump to the user application). */
    while (XMODEM_OK == status) {
        uint8_t header = 0x00u;

        /* Get the header from UART. */
        bool commStatus = uartDriverReceiveByte(&header);

        /* Spam the host (until we receive something) with ACSII "C", to notify it, we want to use CRC-16. */
        if ((!commStatus) && (sFirstPacketReceived == false)) {
            uartDriverSendByte(XMODEM_C);
        }

        /* Uart timeout or any other errors. */
        else if ((!commStatus) && (sFirstPacketReceived == true)) {
            status = ErrorHandler(&errorNumber, XMODEM_MAX_ERRORS);
        }
        else {
            /* Do nothing. */
        }

        /* The header can be: SOH, STX, EOT and CAN. */
        switch (header) {
            xModemStatus_t packetStatus = XMODEM_ERROR;
            /* 128 or 1024 bytes of data. */

        case XMODEM_SOH:
        case XMODEM_STX:

            packetStatus = HandlePacket(header); /* If the handling was successful, then send an ACK. */

            if (packetStatus == XMODEM_OK) {
                uartDriverSendByte(XMODEM_ACK);
            }

            else if (packetStatus == XMODEM_ERROR_FLASH) /* If the error was flash related, then immediately set the error counter to max (graceful abort). */
            {
                errorNumber = XMODEM_MAX_ERRORS;
                status = ErrorHandler(&errorNumber, XMODEM_MAX_ERRORS);
            }
            else /* Error while processing the packet, either send a NAK or do graceful abort. */
            {
                status = ErrorHandler(&errorNumber, XMODEM_MAX_ERRORS);
            }
            break;
            /* End of Transmission. */

        case XMODEM_EOT:
            /* ACK, feedback to user (as a text), then jump to user application. */
            uartDriverSendByte(XMODEM_ACK);

            LOG_INFO("\n\rProgram downloaded to the the external flash\n\r");
            return true;
            break;
            /* Abort from host. */

        case XMODEM_CAN:
            status = XMODEM_ERROR;
            break;

        default:
            /* Wrong header. */
            if (commStatus) {
                status = ErrorHandler(&errorNumber, XMODEM_MAX_ERRORS);
            }
            break;
        }
    }
    return false;
}

static uint16_t CalculateCRC(uint8_t *data, uint16_t length)
{
    uint16_t crc = 0u;
    while (length) {
        length--;
        crc = crc ^ ((uint16_t)*data++ << 8u);
        for (uint8_t i = 0u; i < 8u; i++) {
            if (crc & 0x8000u) {
                crc = (crc << 1u) ^ 0x1021u;
            }
            else {
                crc = crc << 1u;
            }
        }
    }
    return crc;
}

static xModemStatus_t HandlePacket(uint8_t header)
{
    xModemStatus_t status = XMODEM_OK;
    uint16_t size = 0u;
    static uint32_t actualExternalFlashAddress = MX25L512_START_ADDRESS;

    /* 2 bytes for packet number, 1024 for data, 2 for CRC*/
    uint8_t receivedPacketNumber[XMODEM_PACKET_NUMBER_SIZE];
    uint8_t receivedPacketData[XMODEM_PACKET_1024_SIZE];
    uint8_t receivedPacketCRC[XMODEM_PACKET_CRC_SIZE];

    /* Get the size of the data. */
    if (header == XMODEM_SOH) {
        size = XMODEM_PACKET_128_SIZE;
    }
    else if (header == XMODEM_STX) {
        size = XMODEM_PACKET_1024_SIZE;
    }
    else {
        /* Wrong header type. This shoudn't be possible... */
        status |= XMODEM_ERROR;
    }

    bool commStatus = true;
    /* Get the packet number, data and CRC from UART. */
    commStatus |= uartDriverReceiveMultipleBytes(&receivedPacketNumber[0u], XMODEM_PACKET_NUMBER_SIZE);
    commStatus |= uartDriverReceiveMultipleBytes(&receivedPacketData[0u], size);
    commStatus |= uartDriverReceiveMultipleBytes(&receivedPacketCRC[0u], XMODEM_PACKET_CRC_SIZE);
    /* Merge the two bytes of CRC. */
    uint16_t crcReceived = ((uint16_t)receivedPacketCRC[XMODEM_PACKET_CRC_HIGH_INDEX] << 8u) |
                           ((uint16_t)receivedPacketCRC[XMODEM_PACKET_CRC_LOW_INDEX]);
    /* We calculate it too. */
    uint16_t crcCalculated = CalculateCRC(&receivedPacketData[0u], size);

    /* Communication error. */
    if (!commStatus) {
        status |= XMODEM_ERROR_UART;
    }

    /* Error handling and flashing. */
    if (status == XMODEM_OK) {
        if (sPacketNumber != receivedPacketNumber[0u]) {
            /* Packet number counter mismatch. */
            status |= XMODEM_ERROR_NUMBER;
        }
        if ((receivedPacketNumber[XMODEM_PACKET_NUMBER_INDEX] +
             receivedPacketNumber[XMODEM_PACKET_NUMBER_COMPLEMENT_INDEX]) != 255U) {
            /* The sum of the packet number and packet number complement aren't 255. */
            /* The sum always has to be 255. */
            status |= XMODEM_ERROR_NUMBER;
        }
        if (crcCalculated != crcReceived) {
            /* The calculated and received CRC are different. */
            status |= XMODEM_ERROR_CRC;
        }
    }

    /* Do the actual flashing (if there weren't any errors). */
    if (status == XMODEM_OK) {
        if (actualExternalFlashAddress % MX25L512_SUBSECTOR_SIZE == 0) {
            if (QspiFlashEraseBlock(actualExternalFlashAddress) != QSPI_OK) {
                status |= XMODEM_ERROR_FLASH;
            }
            else {
                sFirstPacketReceived = true;
            }
        }
    }

    if ((status == XMODEM_OK) &&
        (QspiFlashWrite(receivedPacketData, actualExternalFlashAddress, (uint32_t)size) != QSPI_OK)) {
        /* Flashing error. */
        status |= XMODEM_ERROR_FLASH;
    }

    /* Raise the packet number and the address counters (if there weren't any errors). */
    if (status == XMODEM_OK) {

        sPacketNumber++;
        actualExternalFlashAddress += size;
        sEndOfExternalFlashFile = actualExternalFlashAddress;
    }

    return status;
}

static xModemStatus_t ErrorHandler(uint8_t *errorNumber, uint8_t maxErrorNumber)
{
    xModemStatus_t status = XMODEM_OK;
    /* Raise the error counter. */
    (*errorNumber)++;
    /* If the counter reached the max value, then abort. */
    if ((*errorNumber) >= maxErrorNumber) {
        /* Graceful abort. */
        uartDriverSendByte(XMODEM_CAN);
        uartDriverSendByte(XMODEM_CAN);
        status = XMODEM_ERROR;
    }
    /* Otherwise send a NAK for a repeat. */
    else {
        uartDriverSendByte(XMODEM_NAK);
        status = XMODEM_OK;
    }
    return status;
}
