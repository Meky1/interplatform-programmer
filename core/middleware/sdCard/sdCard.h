/*****************************************************************************
 * @file sdCard.h
 *
 * @brief Header file for the SD card handler
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <ctype.h>
#include <stdint.h>
#include <string.h>

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/**
 * @brief
 */
void SdCardHandler(char *nameOfFile);
