/*****************************************************************************
 * @file sdCard.c
 *
 * @brief Source file for the SD card handler
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/


#include "middleware/sdCard/sdCard.h"
#include "driver/assertDebug/debug.h"
#include "driver/flashDriver/flashDriver.h"
#include "driver/ledManagement/ledManagement.h"
#include "driver/mcuDriver/mcuDriver.h"
#include "driver/qspiFlashDriver/qspiFlashDriver.h"
#include "driver/sdCardDriver/sdCardDriver.h"
#include "external/mx25l512/mx25l512.h"
#include "middleware/hexParser/hexParser.h"
#include "middleware/inAppProg/inAppProg.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define NUMBER_OF_ATTEMPTS (3U)
#define NUMBER_OF_READ_BYTES (4U)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

static uint32_t sActualExternalFlashAddress = 0u; /**< Address where we have to write. */

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/**
 * @brief   Initialize the SD card and file with given path
 * @param   path: Path of the file to be programmed from the SD card
 * @return  true if initializing was a success, otherwise false
 */
static bool SdCardInit(char *path);

/**
 * @brief   Deinitialize the SD card and file with given path
 */
static void SdCardDeinit(void);

/**
 * @brief   This function flashes the external memory using data saved on the SD card.
 * @return  true if programming was a success, otherwise false
 */
static bool SdCardSaveToExternalFlash(void);

/**
 * @brief   This function checks if the saved file on the SD card is of hex extension
 * @return  true if it is, otherwise false
 */
static bool SdCardCheckIfFileIsHex(char *path);

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

void SdCardHandler(char *nameOfFile)
{
    LOG_INFO("Checking if there is an SD card with application.. \n\r");
    if (SdCardInit(nameOfFile)) {
        LOG_INFO("Application found! Flashing with the given data..\r\n");
        if (SdCardSaveToExternalFlash()) {
            SdCardDeinit();
            LOG_INFO("Flashed external flash with SD card application. Name of file: %s\n\r", nameOfFile);

            if (SdCardCheckIfFileIsHex(nameOfFile)) {
                if (!HexParserProcess()) {
                    LOG_INFO("Hex parser has failed!\n\r");
                }
                sActualExternalFlashAddress = HexParserGetEndOfBinSection();
            }

            if (inAppProgFlashWithUserApp(sActualExternalFlashAddress) != false) {
                LOG_INFO("Jumping to user application..\n\r");
                flashJumpToApp();
            }
        }
        SdCardDeinit();
    }
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static bool SdCardInit(char *path)
{
    if (SdCardMount() != SD_CARD_OK) {
        return false;
    }

    if (SdCardOpenFile(path, SD_OPEN_READ) != SD_CARD_OK) {
        SdCardEject();
        return false;
    }

    if (SdCardGetOpenedFileSize() > FLASH_APP_SIZE) {
        SdCardCloseFile();
        SdCardEject();
        return false;
    }
    return true;
}

static void SdCardDeinit(void)
{
    SdCardCloseFile();
    SdCardEject();
}

static bool SdCardSaveToExternalFlash(void)
{
    uint8_t dataBuff[4] = { 0 };
    uint8_t counter = 0;
    int64_t sizeOfRemainingData = (int64_t)SdCardGetOpenedFileSize();

    sActualExternalFlashAddress = MX25L512_START_ADDRESS;

    for (uint32_t i = MX25L512_START_ADDRESS; i < (MX25L512_FLASH_SIZE / MX25L512_SUBSECTOR_SIZE);
         i = i + MX25L512_SUBSECTOR_SIZE) {
        if (QspiFlashEraseBlock(i) != QSPI_OK) {
            return false;
        }
    }

    while (sizeOfRemainingData > 0) {
        if (SdCardReadFromFile(dataBuff, NUMBER_OF_READ_BYTES) != SD_CARD_OK) {
            if (counter < NUMBER_OF_ATTEMPTS) {
                LOG_INFO("Failed to read from file.. Attempt: %d/%d\r\n");
                counter++;
            }
            else {
                SdCardDeinit();
                LOG_INFO("Failed to read from file..\r\n");
                return false;
            }
        }

        uint32_t dataToSend = 0;
        dataToSend |= (uint32_t)dataBuff[3] << 24;
        dataToSend |= (uint32_t)dataBuff[2] << 16;
        dataToSend |= (uint32_t)dataBuff[1] << 8;
        dataToSend |= (uint32_t)dataBuff[0];

        if (sActualExternalFlashAddress % MX25L512_SUBSECTOR_SIZE == 0) {  // todo: in case of exceeding the space
            if (QspiFlashEraseBlock(sActualExternalFlashAddress) != QSPI_OK) {
                LOG_INFO("Failed to erase block of data from the external flash driver..\r\n");
                return false;
            }
        }

        if (QspiFlashWrite((uint8_t *)&dataToSend, sActualExternalFlashAddress, NUMBER_OF_READ_BYTES) != QSPI_OK) {
            LOG_INFO("Failed to write to external flash driver..\r\n");
            return false;
        }

        sActualExternalFlashAddress = sActualExternalFlashAddress + NUMBER_OF_READ_BYTES;
        sizeOfRemainingData = sizeOfRemainingData - NUMBER_OF_READ_BYTES;
    }

    LOG_INFO("Program downloaded to the the external flash.\n\r");

    return true;
}

static bool SdCardCheckIfFileIsHex(char *path)
{
    char extension[4] = { 0 };
    strncpy(extension, path + strlen(path) - 3, 3);
    for (uint8_t i = 0; i < strlen(extension); i++) {
        extension[i] = toupper(extension[i]);
    }
    if (strncmp(extension, "HEX", 3) == 0) {
        return true;
    }
    return false;
}
