/*****************************************************************************
 * @file inAppProg.c
 *
 * @brief Source file for the IAP system
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "middleware/inAppProg/inAppProg.h"
#include "driver/assertDebug/debug.h"
#include "driver/flashDriver/flashDriver.h"
#include "driver/ledManagement/ledManagement.h"
#include "driver/mcuDriver/mcuDriver.h"
#include "driver/qspiFlashDriver/qspiFlashDriver.h"
#include "driver/uartDriver/uartDriver.h"
#include "external/mx25l512/mx25l512.h"
#include "middleware/hexParser/hexParser.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define FLASH_LOAD_PACKET (1024U)

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

bool inAppProgFlashWithUserApp(uint32_t endOfExternalFlashAddress)
{
    uint8_t numberOfLoopsNeeded = (uint8_t)(endOfExternalFlashAddress / FLASH_LOAD_PACKET) + 1;
    uint32_t actualFlashAddress = FLASH_APP_START_ADDRESS;
    uint32_t actualExternalFlashAddress = MX25L512_START_ADDRESS;
    uint32_t size = FLASH_LOAD_PACKET;
    uint8_t receivedData[FLASH_LOAD_PACKET];

    if (endOfExternalFlashAddress > FLASH_BANK10_END - FLASH_APP_START_ADDRESS) {
        LOG_INFO("Given data too big to be programmed!\n\r");
        return false;
    }

    if (flashEraseApplication() != FLASH_OK) {
        return false;
    }

    for (int i = 0; i < numberOfLoopsNeeded; i++) {
        if (QspiFlashRead(receivedData, actualExternalFlashAddress, size) != QSPI_OK) {
            LOG_INFO("Failed to read data from external flash!\n\r");
            return false;
        }
        LOG_INFO("Read packet from external flash driver. Progress: %d/%d\n\r", i + 1, numberOfLoopsNeeded);

        if (flashWrite(actualFlashAddress, (uint32_t *)&receivedData[0u], (uint32_t)size / 4U) != FLASH_OK) {
            LOG_INFO("Failed to save data in the flash! Address: %x\n\r", actualFlashAddress);

            return false;
        }
        actualFlashAddress += size;
        actualExternalFlashAddress += size;
    }

    return true;
}

fileFormat_t inAppProgReceiveFileType(void)
{
    fileFormat_t fileType = 0;
    LOG_INFO("Waiting for format information.\n\r");
    if (!uartDriverReceiveByteLongTimeout(&fileType)) {
        LOG_INFO("Error in receiving data format.\n\r");
        LOG_INFO("Bin assumed..\n\r");
        return BIN_FILE;
    }

    char typeString[12] = { 0 };

    switch (fileType) {
    case BIN_FILE:
        sprintf(typeString, "Binary file");
        break;
    case HEX_FILE:
        sprintf(typeString, "Hex file");
        break;
    default:
        return NO_FILE;
    }

    LOG_INFO("Format information received: %s\n\r", typeString);

    return fileType;
}

void inAppProgInitializeSystem(void)
{
    if (QspiFlashInit() != QSPI_OK) {
        LOG_INFO("Failed to initialize QSPI flash unit\n\r");
        flashEraseApplication();
        McuDriverSystemReset();
    }
}
