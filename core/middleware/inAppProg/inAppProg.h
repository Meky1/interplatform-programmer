/*****************************************************************************
 * @file inAppProg.h
 *
 * @brief Header file for the IAP system
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

typedef enum { NO_FILE = 0, BIN_FILE = 49, HEX_FILE = 50 } fileFormat_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/**
 * @brief   This function flashes the MCU with program stored in the external flash drive
 * @return  If flash process succeeds, otherwise false
 */
bool inAppProgFlashWithUserApp(uint32_t endOfExternalFlashAddress);

/**
 * @brief   Receive data about the incoming file format
 * @return	Incoming file format
 */
fileFormat_t inAppProgReceiveFileType(void);

/**
 * @brief   Initialize functions
 */
void inAppProgInitializeSystem(void);
