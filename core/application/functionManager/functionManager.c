/*****************************************************************************
 * @file functionManager.c
 *
 * @brief Source file containing the setup starting function and the loop function
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "application/functionManager/functionManager.h"
#include "driver/assertDebug/debug.h"
#include "driver/ledManagement/ledManagement.h"
#include "driver/mcuDriver/mcuDriver.h"
#include "main.h"
#include "middleware/inAppProg/inAppProg.h"
#include "middleware/sdCard/sdCard.h"
#include "middleware/xmodem/xmodem.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define NAME_OF_FILE_ON_SD_CARD ("template.bin")

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/

void FunctionManagementSetup(void)
{
    inAppProgInitializeSystem();

    LOG_INFO("\n\r\n\r\n\r");
    LOG_INFO("===================================================\n\r");
    LOG_INFO("            UART Bootloader for STM32F7\n\r");
    LOG_INFO("        Using xmodem (1k) protocol or SD card\n\r");
    LOG_INFO("===================================================\n\r\n\r");

    SdCardHandler(NAME_OF_FILE_ON_SD_CARD);

    LOG_INFO("SD card programming process has failed\n\r");
    LOG_INFO("Switching to xmodem (1k) flash algorithm\n\r");
}

void FunctionManagementLoop(void)
{
    LedTurnOnGreenLed();

    LOG_INFO("Please send a new binary file with xmodem (1k) protocol to update the firmware.\n\r");

    xmodemHandler();

    LOG_INFO("\n\rFailed to update firmware.. Please try again.\n\r");
    McuDriverSystemReset();
}
