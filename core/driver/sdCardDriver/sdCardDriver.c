/*****************************************************************************
 * @file sdCardDriver.c
 *
 * @brief Source file for the SD card driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "driver/sdCardDriver/sdCardDriver.h"
#include "driver/assertDebug/debug.h"
#include "driver/flashDriver/flashDriver.h"
#include "driver/qspiFlashDriver/qspiFlashDriver.h"
#include "external/mx25l512/mx25l512.h"
#include "fatfs.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define ERROR_BUFFER_SIZE (200U)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

extern char SDPath[4]; /* SD logical drive path */
extern FATFS SDFatFS;  /* File system object for SD logical drive */
extern FIL SDFile;     /* File object for SD */

static char sErrorBuffer[ERROR_BUFFER_SIZE] = { 0 };

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

sdCardStatus_t SdCardEject(void)
{
    sdCardStatus_t status = (sdCardStatus_t)f_mount(NULL, (TCHAR const *)SDPath, 0);
    if (status != SD_CARD_OK) {
        char *statusString = CheckEnumCode(status);
        LOG_INFO("Error has occured while ejecting! Info: %s.\r\n", statusString);
        return status;
    }

    LOG_INFO("SD card was ejected.\r\n");
    return status;
}

sdCardStatus_t SdCardMount(void)
{
    sdCardStatus_t status = (sdCardStatus_t)f_mount(&SDFatFS, (TCHAR const *)SDPath, 1);
    if (status != SD_CARD_OK) {
        char *statusString = CheckEnumCode(status);
        LOG_INFO("Error has occured while mounting! Info: %s.\r\n", statusString);
        return status;
    }

    LOG_INFO("SD card was mounted\r\n");
    return status;
}

sdCardStatus_t SdCardOpenFile(char *path, sdOpenFlag_t flag)
{
    sdCardStatus_t status = (sdCardStatus_t)f_open(&SDFile, path, flag);
    if (status != SD_CARD_OK) {
        char *statusString = CheckEnumCode(status);
        LOG_INFO("Error has occured while opening file %s! Info: %s.\r\n", path, statusString);
        return status;
    }

    LOG_INFO("File %s was opened.\r\n", path);
    return status;
}

sdCardStatus_t SdCardReadFromFile(uint8_t *buffer, uint8_t numberOfBytes)
{
    UINT controlByte;
    UINT numberOfBytesUINT = (UINT)numberOfBytes;
    sdCardStatus_t status = (sdCardStatus_t)f_read(&SDFile, buffer, numberOfBytesUINT, &controlByte);

    if (status != SD_CARD_OK) {
        char *statusString = CheckEnumCode(status);
        LOG_INFO("Error has occured while reading from file! Info: %s.\r\n", statusString);
        return status;
    }

    if (controlByte == 0) {
        LOG_INFO("Read 0 bytes!\r\n");
    }

    return status;
}

sdCardStatus_t SdCardCloseFile(void)
{
    sdCardStatus_t status = (sdCardStatus_t)f_close(&SDFile);
    if (status != SD_CARD_OK) {
        char *statusString = CheckEnumCode(status);
        LOG_INFO("Error has occured while closing file! Info: %s.\r\n", statusString);
        return status;
    }

    LOG_INFO("File was closed.\r\n");
    return status;
}

uint32_t SdCardGetOpenedFileSize(void)
{
    return f_size(&SDFile);
}

char *CheckEnumCode(sdCardStatus_t code)
{
    memset(sErrorBuffer, 0, ERROR_BUFFER_SIZE);

    switch (code) {
    case SD_CARD_INT_ERR:
        sprintf(sErrorBuffer, "Assertion failed.");
        break;
    case SD_CARD_NOT_READY:
        sprintf(sErrorBuffer, "The physical drive cannot work.");
        break;
    case SD_CARD_NO_FILE:
        sprintf(sErrorBuffer, "Could not find the file.");
        break;
    case SD_CARD_NO_PATH:
        sprintf(sErrorBuffer, "Could not find the path.");
        break;
    case SD_CARD_INVALID_NAME:
        sprintf(sErrorBuffer, "The path name format is invalid.");
        break;
    case SD_CARD_DENIED:
        sprintf(sErrorBuffer, "Access denied due to prohibited access or directory full.");
        break;
    case SD_CARD_EXIST:
        sprintf(sErrorBuffer, "Access denied due to prohibited access.");
        break;
    case SD_CARD_INVALID_OBJECT:
        sprintf(sErrorBuffer, "The file/directory object is invalid.");
        break;
    case SD_CARD_WRITE_PROTECTED:
        sprintf(sErrorBuffer, "The physical drive is write protected.");
        break;
    case SD_CARD_INVALID_DRIVE:
        sprintf(sErrorBuffer, "The logical drive number is invalid.");
        break;
    case SD_CARD_NOT_ENABLED:
        sprintf(sErrorBuffer, "The volume has no work area.");
        break;
    case SD_CARD_NO_FILESYSTEM:
        sprintf(sErrorBuffer, "There is no valid FAT volume.");
        break;
    case SD_CARD_MKFS_ABORTED:
        sprintf(sErrorBuffer, "The f_mkfs() aborted due to any problem.");
        break;
    case SD_CARD_TIMEOUT:
        sprintf(sErrorBuffer, "Could not get a grant to access the volume within defined period.");
        break;
    case SD_CARD_LOCKED:
        sprintf(sErrorBuffer, "The operation is rejected according to the file sharing policy.");
        break;
    case SD_CARD_NOT_ENOUGH_CORE:
        sprintf(sErrorBuffer, "LFN working buffer could not be allocated.");
        break;
    case SD_CARD_TOO_MANY_OPEN_FILES:
        sprintf(sErrorBuffer, "Number of open files > _FS_LOCK.");
        break;
    case SD_CARD_INVALID_PARAMETER:
        sprintf(sErrorBuffer, "Given parameter is invalid.");
        break;
    default:
        sprintf(sErrorBuffer, "Unknown error!");
        break;
    }

    return sErrorBuffer;
}
