/*****************************************************************************
 * @file sdCardDriver.h
 *
 * @brief Header file for the SD card driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

typedef enum {
    SD_CARD_OK = 0,          /* (0) Succeeded */
    SD_CARD_DISK_ERR,        /* (1) A hard error occurred in the low level disk I/O layer */
    SD_CARD_INT_ERR,         /* (2) Assertion failed */
    SD_CARD_NOT_READY,       /* (3) The physical drive cannot work */
    SD_CARD_NO_FILE,         /* (4) Could not find the file */
    SD_CARD_NO_PATH,         /* (5) Could not find the path */
    SD_CARD_INVALID_NAME,    /* (6) The path name format is invalid */
    SD_CARD_DENIED,          /* (7) Access denied due to prohibited access or directory full */
    SD_CARD_EXIST,           /* (8) Access denied due to prohibited access */
    SD_CARD_INVALID_OBJECT,  /* (9) The file/directory object is invalid */
    SD_CARD_WRITE_PROTECTED, /* (10) The physical drive is write protected */
    SD_CARD_INVALID_DRIVE,   /* (11) The logical drive number is invalid */
    SD_CARD_NOT_ENABLED,     /* (12) The volume has no work area */
    SD_CARD_NO_FILESYSTEM,   /* (13) There is no valid FAT volume */
    SD_CARD_MKFS_ABORTED,    /* (14) The f_mkfs() aborted due to any problem */
    SD_CARD_TIMEOUT, /* (15) Could not get a grant to access the volume within defined period */
    SD_CARD_LOCKED,  /* (16) The operation is rejected according to the file sharing policy */
    SD_CARD_NOT_ENOUGH_CORE,     /* (17) LFN working buffer could not be allocated */
    SD_CARD_TOO_MANY_OPEN_FILES, /* (18) Number of open files > _FS_LOCK */
    SD_CARD_INVALID_PARAMETER    /* (19) Given parameter is invalid */

} sdCardStatus_t;

typedef enum {
    SD_OPEN_READ = 0x01,
    SD_OPEN_WRITE = 0x02,
    SD_OPEN_OPEN_EXISTING = 0x00,
    SD_OPEN_CREATE_NEW = 0x04,
    SD_OPEN_CREATE_ALWAYS = 0x08,
    SD_OPEN_OPEN_ALWAYS = 0x10,
    SD_OPEN_OPEN_APPEND = 0x30

} sdOpenFlag_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/**
 * @brief   Eject the SD card in the socket
 * @return	Status of the operation
 */
sdCardStatus_t SdCardEject(void);

/**
 * @brief   Mount the SD card in the socket
 * @return	Status of the operation
 */
sdCardStatus_t SdCardMount(void);

/**
 * @brief   Open the file on the SD card
 * @param   path: Path to the file
 * @param   flag: Flag with which the opening should be processed (search for sdOpenFlag_t)
 * @return	Status of the operation
 */
sdCardStatus_t SdCardOpenFile(char *path, sdOpenFlag_t flag);

/**
 * @brief   Read the opened file on the SD card
 * @param   buffer: Buffer in which data read will be stored
 * @param   numberOfBytes: Number of bytes to be read
 * @return	Status of the operation
 */
sdCardStatus_t SdCardReadFromFile(uint8_t *buffer, uint8_t numberOfBytes);

/**
 * @brief   Close the file on the SD card
 * @return	Status of the operation
 */
sdCardStatus_t SdCardCloseFile(void);

/**
 * @brief   Get the size of the opened file
 * @return	Size of the opened file
 */
uint32_t SdCardGetOpenedFileSize(void);

/**
 * @brief   Write data to the opened file
 * @return	Size of the opened file
 */
// static uint32_t SdCardWriteToOpenedFile(uint8_t data);

/**
 * @brief   Convert enum state to a string notifier
 * @return	String notifier
 */
char *CheckEnumCode(sdCardStatus_t code);
