/*****************************************************************************
 * @file swo.h
 *
 * @brief Source file for the SWO log system
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "swo.h"
#include "stm32f7xx_hal.h"

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
*****************************************************************************/

/** @brief  Basic writing function - weak declaration in syscalls.c
 *  @param file File descriptor of file into which data is written.
 *  @param str  Data to be written.
 *  @param len  Number of bytes
 *  @return length of string
 *  */
int _write(int file, char *str, int len);

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

void ITM_WriteCharToPort(uint32_t port, uint32_t character)
{
    while (ITM->PORT[port].u16 == 0 && ITM->PORT[port].u32 == 0)
        ;
    ITM->PORT[port].u8 = (uint8_t)character;
}


void ITM_WriteString(uint32_t port, const char *str)
{
    uint32_t i = 0;
    uint32_t len = strlen(str);
    for (i = 0; i < len; i++) {
        ITM_WriteCharToPort(port, (uint32_t)*str++);
    }
}

void ITM_Write(uint32_t port, const char *str, ...)
{
    va_list ap;

    va_start(ap, str);
    ITM_vWrite(port, str, ap);
    va_end(ap);
}

void ITM_vWrite(uint32_t port, const char *str, va_list list)
{
    static char buffer[512];
    vsprintf(buffer, str, list);
    ITM_WriteString(port, buffer);
    ITM_WriteString(port, "\n");
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
******************************************************************************/

int _write(int file, char *str, int len)
{
    uint32_t i = 0;

    for (i = 0; i < len; i++) {
        ITM_SendChar((*str++));
    }
    return len;
}
