/*****************************************************************************
 * @file swo.h
 *
 * @brief Header file for SWO log system
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/** @brief Function writes a single char to a given port
 *  @param port  Port number
 *  @param ch    Char written to SWO port */
void ITM_WriteCharToPort(uint32_t port, uint32_t ch);

/** @brief Function writes a string to a given port
 *  @param port  Port number
 *  @param str   String written to SWO  */
void ITM_WriteString(uint32_t port, const char *str);

/** @brief The variadic function writes a string buffer, with given arguments to a given port
 *  @param port  Port number
 *  @param str   String written to SWO with given variadic arguments */
void ITM_Write(uint32_t port, const char *str, ...);


/** @brief Function writes a string buffer, with given arguments to a given port
 *  @param port  Port number
 *  @param str   String written to SWO with given variadic arguments
 *  @param list  Given variadic list	*/
void ITM_vWrite(uint32_t port, const char *str, va_list list);
