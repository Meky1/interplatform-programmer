/*****************************************************************************
 * @file mcuDriver.h
 *
 * @brief Abstraction layer for HAL STM32 core driver
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 ****************************************************************************/

#include "mcuDriver.h"

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
******************************************************************************/
__STATIC_INLINE bool IsInInterrupt(void)
{
    return (SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) != 0;
}

__STATIC_INLINE bool IsInDebugMode(void)
{
    return ((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk));
}
/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

void McuDriverSystemReset(void)
{
    HAL_NVIC_SystemReset();
}


void McuDriverEnableIRQ(IRQn_Type IRQn)
{
    HAL_NVIC_EnableIRQ(IRQn);
}


void McuDriverDisableIRQ(IRQn_Type IRQn)
{
    HAL_NVIC_DisableIRQ(IRQn);
}


void McuDriverSetPriority(IRQn_Type IRQn, uint32_t PreemptPriority, uint32_t SubPriority)
{
    HAL_NVIC_SetPriority(IRQn, PreemptPriority, SubPriority);
}


void McuDriverClearPendingIRQ(IRQn_Type IRQn)
{
    HAL_NVIC_ClearPendingIRQ(IRQn);
}


bool McuDriverIsInInterrupt(void)
{
    return IsInInterrupt();
}

bool McuDriverIsInDebugMode(void)
{
    return IsInDebugMode();
}

void McuDriverInsertBreakpoint(void)
{
    if (!McuDriverIsInDebugMode()) {
        return;
    }

    __ASM volatile("BKPT #0");
}
