/*****************************************************************************
 * @file mcuDriver.h
 *
 * @brief Abstraction layer for HAL stm32 core driver
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "stm32f7xx_hal.h"

/*****************************************************************************
                     PUBLIC STRUCTS / ENUMS / VARIABLES
*****************************************************************************/
typedef enum {
    MCU_TIMER_SYSTEM_STATS = 0,
} timerId_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/
void McuDriverSystemReset(void);

void McuDriverEnableIRQ(IRQn_Type IRQn);

void McuDriverDisableIRQ(IRQn_Type IRQn);

void McuDriverSetPriority(IRQn_Type IRQn, uint32_t PreemptPriority, uint32_t SubPriority);

void McuDriverClearPendingIRQ(IRQn_Type IRQn);

bool McuDriverIsInInterrupt(void);

bool McuDriverIsInDebugMode(void);

void McuDriverInsertBreakpoint(void);
