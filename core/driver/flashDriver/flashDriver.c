/*****************************************************************************
 * @file flashDriver.c
 *
 * @brief Source file for the flash driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "driver/flashDriver/flashDriver.h"
#include "main.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define ADDR_FLASH_SECTOR_0 ((uint32_t)0x08000000)  /* Base @ of Sector 0, 32 Kbyte */
#define ADDR_FLASH_SECTOR_1 ((uint32_t)0x08008000)  /* Base @ of Sector 1, 32 Kbyte */
#define ADDR_FLASH_SECTOR_2 ((uint32_t)0x08010000)  /* Base @ of Sector 2, 32 Kbyte */
#define ADDR_FLASH_SECTOR_3 ((uint32_t)0x08018000)  /* Base @ of Sector 3, 32 Kbyte */
#define ADDR_FLASH_SECTOR_4 ((uint32_t)0x08020000)  /* Base @ of Sector 4, 128 Kbyte */
#define ADDR_FLASH_SECTOR_5 ((uint32_t)0x08040000)  /* Base @ of Sector 5, 256 Kbyte */
#define ADDR_FLASH_SECTOR_6 ((uint32_t)0x08080000)  /* Base @ of Sector 6, 256 Kbyte */
#define ADDR_FLASH_SECTOR_7 ((uint32_t)0x080C0000)  /* Base @ of Sector 7, 256 Kbyte */
#define ADDR_FLASH_SECTOR_8 ((uint32_t)0x08100000)  /* Base @ of Sector 8, 256 Kbyte */
#define ADDR_FLASH_SECTOR_9 ((uint32_t)0x08140000)  /* Base @ of Sector 9, 256 Kbyte */
#define ADDR_FLASH_SECTOR_10 ((uint32_t)0x08180000) /* Base @ of Sector 10, 256 Kbyte */
#define ADDR_FLASH_SECTOR_11 ((uint32_t)0x081C0000) /* Base @ of Sector 11, 256 Kbyte */

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

/* Function pointer for jumping to user application. */
typedef void (*jmp_ptr)(void);

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/**
 * @brief   Get the sector of the memory in which the address is stored
 * @param   Address of the memory
 * @return  Returns the sector
 */
static uint32_t GetSector(uint32_t Address);

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

flashStatus_t flashEraseApplication(void)
{
    flashStatus_t status = FLASH_ERROR;
    uint32_t UserStartSector;
    uint32_t NumberOfSectors;
    uint32_t SectorError;
    FLASH_EraseInitTypeDef pEraseInit;

    /* Unlock the Flash to enable the flash control register access *************/
    HAL_FLASH_Unlock();

    /* Get the sector where start the user flash area */
    UserStartSector = GetSector(FLASH_APP_START_ADDRESS);
    NumberOfSectors = GetSector(FLASH_APP_END_ADDRESS) - UserStartSector + 1;

    pEraseInit.TypeErase = FLASH_TYPEERASE_SECTORS;
    pEraseInit.Sector = UserStartSector;
    pEraseInit.NbSectors = NumberOfSectors;
    pEraseInit.VoltageRange = VOLTAGE_RANGE_3;

    if (HAL_FLASHEx_Erase(&pEraseInit, &SectorError) == HAL_OK) {
        /* Error occurred while page erase */
        status = FLASH_OK;
        FLASH->CR;
    }

    HAL_FLASH_Lock();

    return status;
}

flashStatus_t flashWrite(uint32_t address, uint32_t *data, uint32_t length)
{
    flashStatus_t status = FLASH_OK;

    HAL_FLASH_Unlock();

    /* Loop through the array. */
    for (uint32_t i = 0u; (i < length) && (FLASH_OK == status); i++) {
        /* If we reached the end of the memory, then report an error and don't do anything else.*/
        if (FLASH_APP_END_ADDRESS <= address) {
            status |= FLASH_SIZE_ERROR;
        }
        else {
            /* The actual flashing. If there is an error, then report it. */
            if (HAL_OK != HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address, data[i])) {
                status |= FLASH_WRITE_ERROR;
            }
            /* Read back the content of the memory. If it is wrong, then report an error. */
            if (((data[i])) != (*(volatile uint32_t *)address)) {
                status |= FLASH_READBACK_ERROR;
            }

            /* Shift the address by a word. */
            address += 4u;
        }
    }

    CLEAR_BIT(FLASH->CR, (FLASH_CR_PG));
    HAL_FLASH_Lock();

    return status;
}

void flashJumpToApp(void)
{
    /* Function pointer to the address of the user application. */
    jmp_ptr jumpToApp;
    jumpToApp = (jmp_ptr)(*(volatile uint32_t *)(FLASH_APP_START_ADDRESS + 4u));

    HAL_DeInit();
    //__disable_irq();

    /* Change the main stack pointer. */
    __set_MSP(*(volatile uint32_t *)FLASH_APP_START_ADDRESS);

    jumpToApp();
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static uint32_t GetSector(uint32_t Address)
{
    uint32_t sector = 0;

    if ((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0)) {
        sector = FLASH_SECTOR_0;
    }
    else if ((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1)) {
        sector = FLASH_SECTOR_1;
    }
    else if ((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2)) {
        sector = FLASH_SECTOR_2;
    }
    else if ((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3)) {
        sector = FLASH_SECTOR_3;
    }
    else if ((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4)) {
        sector = FLASH_SECTOR_4;
    }
    else if ((Address < ADDR_FLASH_SECTOR_6) && (Address >= ADDR_FLASH_SECTOR_5)) {
        sector = FLASH_SECTOR_5;
    }
    else if ((Address < ADDR_FLASH_SECTOR_7) && (Address >= ADDR_FLASH_SECTOR_6)) {
        sector = FLASH_SECTOR_6;
    }
    else if ((Address < ADDR_FLASH_SECTOR_8) && (Address >= ADDR_FLASH_SECTOR_7)) {
        sector = FLASH_SECTOR_7;
    }
    else if ((Address < ADDR_FLASH_SECTOR_9) && (Address >= ADDR_FLASH_SECTOR_8)) {
        sector = FLASH_SECTOR_8;
    }
    else if ((Address < ADDR_FLASH_SECTOR_10) && (Address >= ADDR_FLASH_SECTOR_9)) {
        sector = FLASH_SECTOR_9;
    }
    else if ((Address < ADDR_FLASH_SECTOR_11) && (Address >= ADDR_FLASH_SECTOR_10)) {
        sector = FLASH_SECTOR_10;
    }
    else {
        sector = FLASH_SECTOR_11;
    }

    return sector;
}
