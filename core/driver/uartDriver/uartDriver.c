/*****************************************************************************
 * @file uartDriver.c
 *
 * @brief Source file for the UART driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "driver/uartDriver/uartDriver.h"
#include "driver/assertDebug/debug.h"
#include "main.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define TX_LOG_MAX_LENGTH (1024U)
#define UART_TIMEOUT (1000u)
#define LONG_UART_TIMEOUT (10000u)


/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

extern UART_HandleTypeDef huart1;

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

void uartDriverPrint(char *stringToSend)
{
    HAL_UART_Transmit(&huart1, (uint8_t *)stringToSend, strlen(stringToSend), 10);
}

// Note: This is temporary, needs to be done on another USART channel (conflict with xmodem (1k)),
//		 however due to board conditions it is now easier this way,
//       without additional hardware
void uartDriverPrintLog(const char *format, va_list args)
{
    char bufferTx[TX_LOG_MAX_LENGTH] = { 0 };
    vsnprintf(bufferTx, TX_LOG_MAX_LENGTH, format, args);

    if (HAL_UART_Transmit(&huart1, (uint8_t *)bufferTx, strlen(bufferTx), 10) != HAL_OK) {
        LOG_SWO(0, "UART logging message failed!\n");
    }
}

bool uartDriverSendByte(uint8_t pData)
{
    bool status = false;

    if (HAL_UART_STATE_TIMEOUT == HAL_UART_GetState(&huart1)) {
        HAL_UART_Abort(&huart1);
    }

    if (HAL_UART_Transmit(&huart1, &pData, 1, UART_TIMEOUT) == HAL_OK) {
        status = true;
    }

    return status;
}

bool uartDriverReceiveByte(uint8_t *pData)
{
    return (HAL_UART_Receive(&huart1, pData, 1, UART_TIMEOUT) == HAL_OK);
}

bool uartDriverReceiveByteLongTimeout(uint8_t *pData)
{
    return (HAL_UART_Receive(&huart1, pData, 1, LONG_UART_TIMEOUT) == HAL_OK);
}

bool uartDriverReceiveMultipleBytes(uint8_t *pData, uint32_t length)
{
    return (HAL_UART_Receive(&huart1, pData, length, UART_TIMEOUT) == HAL_OK);
}

void uartDriverFlushDataRegister(void)
{
    __HAL_UART_FLUSH_DRREGISTER(&huart1);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if (huart == &huart1) {
        // TODO:
    }
}
