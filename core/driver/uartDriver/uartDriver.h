/*****************************************************************************
 * @file uartDriver.h
 *
 * @brief Header file for the UART driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

#define TX_TIMEOUT ((uint32_t)100)
#define RX_TIMEOUT ((uint32_t)0xFFFFFFFF)

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/
/**
 * @brief   This function flashes the memory.
 * @param   address: First address to be written to.
 * @return  status: Report about the success of the writing.
 */

/**
 * @brief   Send the given string to the terminal
 * @param   stringToSend: String to be sent to the terminal.
 */
void uartDriverPrint(char *stringToSend);

/**
 * @brief   Send the given string to the terminal using variadic list
 * @param   format: String to be sent to the terminal.
 * @param	args: variadic list containing arguments
 */
void uartDriverPrintLog(const char *format, va_list args);

/**
 * @brief   Send a byte
 * @param   pData: Data to be sent
 * @return  true if byte sent correctly, otherwise false
 */
bool uartDriverSendByte(uint8_t pData);

/**
 * @brief   Receive a byte
 * @param   pData: Pointer to the received data
 * @return  true if byte received correctly, otherwise false
 */
bool uartDriverReceiveByte(uint8_t *pData);

/**
 * @brief   Receive a byte with long timeout
 * @param   pData: Pointer to the received data
 * @return  true if byte received correctly, otherwise false
 */
bool uartDriverReceiveByteLongTimeout(uint8_t *pData);

/**
 * @brief   Receive multiple bytes
 * @param   pData: Pointer to the received data
 * @param   length: Number of data going to be received
 * @return  true if bytes received correctly, otherwise false
 */
bool uartDriverReceiveMultipleBytes(uint8_t *pData, uint32_t length);

/**
 * @brief   Flush the data register
 */
void uartDriverFlushDataRegister(void);
