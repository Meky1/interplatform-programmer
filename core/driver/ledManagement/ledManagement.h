/*****************************************************************************
 * @file ledManagement.h
 *
 * @brief Header file of the LED system
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdio.h>

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/** @brief 	Turn on the green LED
 */
void LedTurnOnGreenLed(void);

/** @brief 	Turn off the green LED
 */
void LedTurnOffGreenLed(void);

/** @brief Toggle the green LED
 */
void LedToggleGreenLed(void);

/** @brief 	Callback from the button
 */
bool ButtonReadState(void);
