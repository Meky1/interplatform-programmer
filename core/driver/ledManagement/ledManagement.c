/*****************************************************************************
 * @file ledManagement.c
 *
 * @brief Source file of the LED system
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "driver/ledManagement/ledManagement.h"
#include "main.h"

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/

void LedTurnOnGreenLed(void)
{
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_SET);
}

void LedTurnOffGreenLed(void)
{
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_RESET);
}

void LedToggleGreenLed(void)
{
    HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
}

bool ButtonReadState(void)
{
    if (HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin) == GPIO_PIN_SET) {
        return true;
    }
    return false;
}
