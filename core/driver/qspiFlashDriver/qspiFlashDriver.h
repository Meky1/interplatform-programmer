/*****************************************************************************
 * @file qspiFlashDriver.h
 *
 * @brief Header file for the external flash driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 ****************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

typedef enum {

    QSPI_OK = 0,
    QSPI_ERROR,
    QSPI_BUSY,
    QSPI_NOT_SUPPORTED,
    QSPI_SUSPENDED

} qspiErrorStatus_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/**
 * @brief  Initialize the QSPI interface.
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashInit(void);

/**
 * @brief  De-Initialize the QSPI interface.
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashDeInit(void);

/**
 * @brief  Reads an amount of data from the QSPI memory.
 * @param  pData: Pointer to data to be read
 * @param  readAddr: Read start address
 * @param  size: Size of data to read
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashRead(uint8_t *pData, uint32_t readAddr, uint32_t size);

/**
 * @brief  Writes an amount of data to the QSPI memory.
 * @param  pData: Pointer to data to be written
 * @param  writeAddr: Write start address
 * @param  size: Size of data to write
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashWrite(uint8_t *pData, uint32_t writeAddr, uint32_t size);

/**
 * @brief  Erases the specified block of the QSPI memory.
 * @param  blockAddress: Block address to erase
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashEraseBlock(uint32_t blockAddress);

/**
 * @brief  Erases the entire QSPI memory.
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashEraseChip(void);

/**
 * @brief  Reads current status of the QSPI memory.
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashGetStatus(void);

/**
 * @brief  Return the configuration of the QSPI memory.
 * @param  pInfo: pointer on the configuration structure
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashGetInfo(void);

/**
 * @brief  Configure the QSPI in memory-mapped mode
 * @retval QSPI memory status
 */
qspiErrorStatus_t QspiFlashEnableMemoryMappedMode(void);
