/*****************************************************************************
 * @file qspiFlashDriver.c
 *
 * @brief Source file for the external flash driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 ****************************************************************************/

#include "driver/qspiFlashDriver/qspiFlashDriver.h"
#include "driver/assertDebug/debug.h"
#include "external/mx25l512/mx25l512.h"
#include "main.h"


/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

typedef struct {

    uint32_t flashSize;          /*!< Size of the flash */
    uint32_t eraseSectorSize;    /*!< Size of sectors for the erase operation */
    uint32_t eraseSectorsNumber; /*!< Number of sectors for the erase operation */
    uint32_t progPageSize;       /*!< Size of pages for the program operation */
    uint32_t progPagesNumber;    /*!< Number of pages for the program operation */

} qspiStatusInfo_t;

extern QSPI_HandleTypeDef hqspi;
static qspiStatusInfo_t sInfoStructure = { 0 };

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/**
 * @brief This function resets the QSPI memory
 * @retval Error status of the function
 */
static qspiErrorStatus_t ResetMemory(void);

/**
 * @brief This function set the QSPI memory in 4-byte address mode
 * @retval Error status of the function
 */
static qspiErrorStatus_t EnterFourBytesAddress(void);

/**
 * @brief  This function configure the dummy cycles on memory side.
 * @retval Error status of the function
 */
static qspiErrorStatus_t DummyCyclesConfig(void);

/**
 * @brief  This function put QSPI memory in QPI mode (quad I/O).
 * @retval Error status of the function
 */
static qspiErrorStatus_t EnterMemoryQpi(void);

/**
 * @brief  This function put QSPI memory in SPI mode.
 * @retval Error status of the function
 */
static qspiErrorStatus_t ExitMemoryQpi(void);

/**
 * @brief  This function configure the Output driver strength on memory side.
 * @retval Error status of the function
 */
static qspiErrorStatus_t OutDriverStrengthConfig(void);

/**
 * @brief  This function send a Write Enable and wait it is effective.
 * @retval Error status of the function
 */
static qspiErrorStatus_t WriteEnable(void);

/**
 * @brief  This function read the SR of the memory and wait the EOP.
 * @retval Error status of the function
 */
static qspiErrorStatus_t AutoPollingMemReady(uint32_t timeout);

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

qspiErrorStatus_t QspiFlashInit(void)
{
    /* QSPI memory reset */
    if (ResetMemory() != QSPI_OK) {
        return QSPI_NOT_SUPPORTED;
    }

    /* Put QSPI memory in QPI mode */
    if (EnterMemoryQpi() != QSPI_OK) {
        return QSPI_NOT_SUPPORTED;
    }

    /* Set the QSPI memory in 4-bytes address mode */
    if (EnterFourBytesAddress() != QSPI_OK) {
        return QSPI_NOT_SUPPORTED;
    }

    /* Configuration of the dummy cycles on QSPI memory side */
    if (DummyCyclesConfig() != QSPI_OK) {
        return QSPI_NOT_SUPPORTED;
    }

    /* Configuration of the Output driver strength on memory side */
    if (OutDriverStrengthConfig() != QSPI_OK) {
        return QSPI_NOT_SUPPORTED;
    }

    return QSPI_OK;
}

uint8_t QspiFlashDeInit(void)
{
    hqspi.Instance = QUADSPI;

    /* Put QSPI memory in SPI mode */
    if (ExitMemoryQpi() != QSPI_OK) {
        return QSPI_NOT_SUPPORTED;
    }

    /* Call the DeInit function to reset the driver */
    if (HAL_QSPI_DeInit(&hqspi) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* System level De-initialization */
    HAL_QSPI_MspDeInit(&hqspi);

    return QSPI_OK;
}

uint8_t QspiFlashRead(uint8_t *pData, uint32_t readAddr, uint32_t size)
{
    QSPI_CommandTypeDef commandStructure;

    /* Initialize the read command */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = QPI_READ_4_BYTE_ADDR_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_4_LINES;
    commandStructure.AddressSize = QSPI_ADDRESS_32_BITS;
    commandStructure.Address = readAddr;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = MX25L512_DUMMY_CYCLES_READ_QUAD_IO;
    commandStructure.NbData = size;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Set S# timing for Read command */
    MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_1_CYCLE);

    /* Reception of the data */
    if (HAL_QSPI_Receive(&hqspi, pData, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Restore S# timing for nonRead commands */
    MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_4_CYCLE);

    return QSPI_OK;
}

uint8_t QspiFlashWrite(uint8_t *pData, uint32_t writeAddr, uint32_t size)
{
    QSPI_CommandTypeDef commandStructure;
    uint32_t endAddress;
    uint32_t currentSize;
    uint32_t currentAddress;

    /* Calculation of the size between the write address and the end of the page */
    currentSize = MX25L512_PAGE_SIZE - (writeAddr % MX25L512_PAGE_SIZE);

    /* Check if the size of the data is less than the remaining place in the page */
    if (currentSize > size) {
        currentSize = size;
    }

    /* Initialize the address variables */
    currentAddress = writeAddr;
    endAddress = writeAddr + size;

    /* Initialize the program command */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = QPI_PAGE_PROG_4_BYTE_ADDR_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_4_LINES;
    commandStructure.AddressSize = QSPI_ADDRESS_32_BITS;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Perform the write page by page */
    do {
        commandStructure.Address = currentAddress;
        commandStructure.NbData = currentSize;

        /* Enable write operations */
        if (WriteEnable() != QSPI_OK) {
            return QSPI_ERROR;
        }

        /* Configure the command */
        if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
            return QSPI_ERROR;
        }

        /* Transmission of the data */
        if (HAL_QSPI_Transmit(&hqspi, pData, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
            return QSPI_ERROR;
        }

        /* Configure automatic polling mode to wait for end of program */
        if (AutoPollingMemReady(HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != QSPI_OK) {
            return QSPI_ERROR;
        }

        /* Update the address and size variables for next page programming */
        currentAddress += currentSize;
        pData += currentSize;
        currentSize = ((currentAddress + MX25L512_PAGE_SIZE) > endAddress) ? (endAddress - currentAddress) :
                                                                             MX25L512_PAGE_SIZE;
    } while (currentAddress < endAddress);

    return QSPI_OK;
}

uint8_t QspiFlashEraseBlock(uint32_t blockAddress)
{
    QSPI_CommandTypeDef commandStructure;

    /* Initialize the erase command */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = SUBSECTOR_ERASE_4_BYTE_ADDR_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_4_LINES;
    commandStructure.AddressSize = QSPI_ADDRESS_32_BITS;
    commandStructure.Address = blockAddress;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_NONE;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    HAL_Delay(1);
    /* Enable write operations */
    if (WriteEnable() != QSPI_OK) {
        return QSPI_ERROR;
    }

    /* Send the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Configure automatic polling mode to wait for end of erase */
    if (AutoPollingMemReady(MX25L512_SUBSECTOR_ERASE_MAX_TIME) != QSPI_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}

uint8_t QspiFlashEraseChip(void)
{
    QSPI_CommandTypeDef commandStructure;

    /* Initialize the erase command */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = BULK_ERASE_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_NONE;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    HAL_Delay(1);
    /* Enable write operations */
    if (WriteEnable() != QSPI_OK) {
        return QSPI_ERROR;
    }

    /* Send the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Configure automatic polling mode to wait for end of erase */
    if (AutoPollingMemReady(MX25L512_BULK_ERASE_MAX_TIME) != QSPI_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}

uint8_t QspiFlashGetStatus(void)
{
    QSPI_CommandTypeDef commandStructure;
    uint8_t reg;

    /* Initialize the read flag status register command */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 1;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Reception of the data */
    if (HAL_QSPI_Receive(&hqspi, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Check the value of the register*/
    if ((reg & MX25L512_SR_WIP) == 0) {
        return QSPI_OK;
    }
    else {
        return QSPI_BUSY;
    }
}

uint8_t QspiFlashGetInfo(void)
{
    /* Configure the structure with the memory configuration */
    sInfoStructure.flashSize = MX25L512_FLASH_SIZE;
    sInfoStructure.eraseSectorSize = MX25L512_SUBSECTOR_SIZE;
    sInfoStructure.eraseSectorsNumber = (MX25L512_FLASH_SIZE / MX25L512_SUBSECTOR_SIZE);
    sInfoStructure.progPageSize = MX25L512_PAGE_SIZE;
    sInfoStructure.progPagesNumber = (MX25L512_FLASH_SIZE / MX25L512_PAGE_SIZE);

    return QSPI_OK;
}

uint8_t QspiFlashEnableMemoryMappedMode(void)
{
    QSPI_CommandTypeDef commandStructure;
    QSPI_MemoryMappedTypeDef memoryMappedConfig;

    /* Configure the command for the read instruction */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = QPI_READ_4_BYTE_ADDR_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_4_LINES;
    commandStructure.AddressSize = QSPI_ADDRESS_32_BITS;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = MX25L512_DUMMY_CYCLES_READ_QUAD_IO;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Configure the memory mapped mode */
    memoryMappedConfig.TimeOutActivation = QSPI_TIMEOUT_COUNTER_DISABLE;
    memoryMappedConfig.TimeOutPeriod = 0;

    if (HAL_QSPI_MemoryMapped(&hqspi, &commandStructure, &memoryMappedConfig) != HAL_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static qspiErrorStatus_t ResetMemory(void)
{
    QSPI_CommandTypeDef commandStructure;
    QSPI_AutoPollingTypeDef configStructure;
    uint8_t reg;

    /* Send command RESET command in QPI mode (QUAD I/Os) */
    /* Initialize the reset enable command */

    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = RESET_ENABLE_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_NONE;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Send the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }
    /* Send the reset memory command */
    commandStructure.Instruction = RESET_MEMORY_CMD;
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Send command RESET command in SPI mode */
    /* Initialize the reset enable command */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    commandStructure.Instruction = RESET_ENABLE_CMD;
    /* Send the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }
    /* Send the reset memory command */
    commandStructure.Instruction = RESET_MEMORY_CMD;
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* After reset CMD, 1000ms requested if QSPI memory SWReset occured during full chip erase operation */
    HAL_Delay(1000);

    /* Configure automatic polling mode to wait the WIP bit=0 */
    configStructure.Match = 0;
    configStructure.Mask = MX25L512_SR_WIP;
    configStructure.MatchMode = QSPI_MATCH_MODE_AND;
    configStructure.StatusBytesSize = 1;
    configStructure.Interval = 0x10;
    configStructure.AutomaticStop = QSPI_AUTOMATIC_STOP_ENABLE;

    commandStructure.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.DataMode = QSPI_DATA_1_LINE;

    if (HAL_QSPI_AutoPolling(&hqspi, &commandStructure, &configStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Initialize the reading of status register */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_1_LINE;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 1;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Reception of the data */
    if (HAL_QSPI_Receive(&hqspi, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Enable write operations, command in 1 bit */
    /* Enable write operations */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    commandStructure.Instruction = WRITE_ENABLE_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_NONE;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Configure automatic polling mode to wait for write enabling */
    configStructure.Match = MX25L512_SR_WREN;
    configStructure.Mask = MX25L512_SR_WREN;
    configStructure.MatchMode = QSPI_MATCH_MODE_AND;
    configStructure.StatusBytesSize = 1;
    configStructure.Interval = 0x10;
    configStructure.AutomaticStop = QSPI_AUTOMATIC_STOP_ENABLE;

    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.DataMode = QSPI_DATA_1_LINE;

    if (HAL_QSPI_AutoPolling(&hqspi, &commandStructure, &configStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Update the configuration register with new dummy cycles */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    commandStructure.Instruction = WRITE_STATUS_CFG_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_1_LINE;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 1;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Enable the Quad IO on the QSPI memory (Non-volatile bit) */
    reg |= MX25L512_SR_QUADEN;

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Transmission of the data */
    if (HAL_QSPI_Transmit(&hqspi, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* 40ms  Write Status/Configuration Register Cycle Time */
    HAL_Delay(40);

    return QSPI_OK;
}

static uint8_t EnterFourBytesAddress(void)
{
    QSPI_CommandTypeDef commandStructure;

    /* Initialize the command */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = ENTER_4_BYTE_ADDR_MODE_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_NONE;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Enable write operations */
    if (WriteEnable() != QSPI_OK) {
        return QSPI_ERROR;
    }

    /* Send the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Configure automatic polling mode to wait the memory is ready */
    if (AutoPollingMemReady(HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != QSPI_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}

static qspiErrorStatus_t DummyCyclesConfig(void)
{
    QSPI_CommandTypeDef commandStructure;
    uint8_t reg[2];

    /* Initialize the reading of status register */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 1;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Reception of the data */
    if (HAL_QSPI_Receive(&hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Initialize the reading of configuration register */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = READ_CFG_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 1;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Reception of the data */
    if (HAL_QSPI_Receive(&hqspi, &(reg[1]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Enable write operations */
    if (WriteEnable() != QSPI_OK) {
        return QSPI_ERROR;
    }

    /* Update the configuration register with new dummy cycles */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = WRITE_STATUS_CFG_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 2;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* MX25L512_DUMMY_CYCLES_READ_QUAD = 3 for 10 cycles in QPI mode */
    MODIFY_REG(reg[1], MX25L512_CR_NB_DUMMY,
               (MX25L512_DUMMY_CYCLES_READ_QUAD << POSITION_VAL(MX25L512_CR_NB_DUMMY)));

    /* Configure the write volatile configuration register command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Transmission of the data */
    if (HAL_QSPI_Transmit(&hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* 40ms  Write Status/Configuration Register Cycle Time */
    HAL_Delay(40);

    return QSPI_OK;
}

static qspiErrorStatus_t EnterMemoryQpi(void)
{
    QSPI_CommandTypeDef commandStructure;
    QSPI_AutoPollingTypeDef configStructure;

    /* Initialize the QPI enable command */
    /* QSPI memory is supported to be in SPI mode, so CMD on 1 LINE */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    commandStructure.Instruction = ENTER_QUAD_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_NONE;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Send the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Configure automatic polling mode to wait the QUADEN bit=1 and WIP bit=0 */
    configStructure.Match = MX25L512_SR_QUADEN;
    configStructure.Mask = MX25L512_SR_QUADEN | MX25L512_SR_WIP;
    configStructure.MatchMode = QSPI_MATCH_MODE_AND;
    configStructure.StatusBytesSize = 1;
    configStructure.Interval = 0x10;
    configStructure.AutomaticStop = QSPI_AUTOMATIC_STOP_ENABLE;

    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.DataMode = QSPI_DATA_4_LINES;

    if (HAL_QSPI_AutoPolling(&hqspi, &commandStructure, &configStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}

static qspiErrorStatus_t ExitMemoryQpi(void)
{
    QSPI_CommandTypeDef commandStructure;

    /* Initialize the QPI enable command */
    /* QSPI memory is supported to be in QPI mode, so CMD on 4 LINES */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = EXIT_QUAD_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_NONE;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Send the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}

static qspiErrorStatus_t OutDriverStrengthConfig(void)
{
    QSPI_CommandTypeDef commandStructure;
    uint8_t reg[2];

    /* Initialize the reading of status register */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 1;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Reception of the data */
    if (HAL_QSPI_Receive(&hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Initialize the reading of configuration register */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = READ_CFG_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 1;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Reception of the data */
    if (HAL_QSPI_Receive(&hqspi, &(reg[1]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Enable write operations */
    if (WriteEnable() != QSPI_OK) {
        return QSPI_ERROR;
    }

    /* Update the configuration register with new output driver strength */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = WRITE_STATUS_CFG_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.NbData = 2;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    /* Set Output Strength of the QSPI memory 15 ohms */
    MODIFY_REG(reg[1], MX25L512_CR_ODS, (MX25L512_CR_ODS_15 << POSITION_VAL(MX25L512_CR_ODS)));

    /* Configure the write volatile configuration register command */
    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Transmission of the data */
    if (HAL_QSPI_Transmit(&hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}

static qspiErrorStatus_t WriteEnable(void)
{
    QSPI_CommandTypeDef commandStructure;
    QSPI_AutoPollingTypeDef configStructure;

    /* Enable write operations */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = WRITE_ENABLE_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_NONE;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    if (HAL_QSPI_Command(&hqspi, &commandStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    /* Configure automatic polling mode to wait for write enabling */
    configStructure.Match = MX25L512_SR_WREN;
    configStructure.Mask = MX25L512_SR_WREN;
    configStructure.MatchMode = QSPI_MATCH_MODE_AND;
    configStructure.StatusBytesSize = 1;
    configStructure.Interval = 0x10;
    configStructure.AutomaticStop = QSPI_AUTOMATIC_STOP_ENABLE;

    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.DataMode = QSPI_DATA_4_LINES;

    if (HAL_QSPI_AutoPolling(&hqspi, &commandStructure, &configStructure, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}

static qspiErrorStatus_t AutoPollingMemReady(uint32_t Timeout)
{
    QSPI_CommandTypeDef commandStructure;
    QSPI_AutoPollingTypeDef configStructure;

    /* Configure automatic polling mode to wait for memory ready */
    commandStructure.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    commandStructure.Instruction = READ_STATUS_REG_CMD;
    commandStructure.AddressMode = QSPI_ADDRESS_NONE;
    commandStructure.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    commandStructure.DataMode = QSPI_DATA_4_LINES;
    commandStructure.DummyCycles = 0;
    commandStructure.DdrMode = QSPI_DDR_MODE_DISABLE;
    commandStructure.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    commandStructure.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    configStructure.Match = 0;
    configStructure.Mask = MX25L512_SR_WIP;
    configStructure.MatchMode = QSPI_MATCH_MODE_AND;
    configStructure.StatusBytesSize = 1;
    configStructure.Interval = 0x10;
    configStructure.AutomaticStop = QSPI_AUTOMATIC_STOP_ENABLE;

    if (HAL_QSPI_AutoPolling(&hqspi, &commandStructure, &configStructure, Timeout) != HAL_OK) {
        return QSPI_ERROR;
    }

    return QSPI_OK;
}
