/*****************************************************************************
 * @file debug.h
 *
 * @brief Header file of the debug implementation - redirection to printf
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include "main.h"
#include <stdarg.h>
#include <stdint.h>

/*** CONFIG ****************************************************************/

// Special attributes for clang-tidy
#ifdef __clang_analyzer__
#define NORETURN __attribute__((analyzer_noreturn))
#else
#define NORETURN
#endif

#if !defined(CFG_BUILD_TYPE) || (CFG_BUILD_TYPE == 0)
//#error Include config.h before including debug.h
#endif

#define CFG_DEBUG_SWO_EN 1
#if CFG_DEBUG_SWO_EN

/** @brief Print to SWO */
void _dbg_swo_printf(uint32_t channel, const char *format, ...);

#define LOG_SWO _dbg_swo_printf
#define __DEBUG_INTERNAL_SWO_PRINTF _dbg_swo_printf

#else
#define LOG_SWO(...)
#define __DEBUG_INTERNAL_SWO_PRINTF(...)
#endif


#define CFG_DEBUG_USART_EN 1
#if CFG_DEBUG_USART_EN

/** @brief Print to USART */
void _dbg_usart_printf(const char *format, ...);

#define __DEBUG_INTERNAL_USART_PRINTF _dbg_usart_printf
#else
#define __DEBUG_INTERNAL_USART_PRINTF(...)
#endif


#define __DBG_PRINTF(...)                                                                          \
    do {                                                                                           \
        __DEBUG_INTERNAL_SWO_PRINTF(0, __VA_ARGS__);                                               \
        __DEBUG_INTERNAL_USART_PRINTF(__VA_ARGS__);                                                \
    } while (0)

#define __PRINTF1(...) (__DEBUG_INTERNAL_SWO_PRINTF(1, __VA_ARGS__))

/*** LOGGING ***************************************************************/

#ifndef CFG_DEBUG_MODULE_NAME
#define CFG_DEBUG_MODULE_NAME "(INTERPROG)"

#ifdef CFG_DEBUG_LOG_EN
#undef CFG_DEBUG_LOG_EN
#endif

#define CFG_DEBUG_LOG_EN 1
#endif

#if CFG_DEBUG_LOG_EN

#define LOG_INFO(...) __DBG_PRINTF("[INFO]  " CFG_DEBUG_MODULE_NAME ": " __VA_ARGS__)
#define LOG_WARN(...) __DBG_PRINTF("[WARN]  " CFG_DEBUG_MODULE_NAME ": " __VA_ARGS__)
#define LOG_ERROR(...) __DBG_PRINTF("[ERROR] " CFG_DEBUG_MODULE_NAME ": " __VA_ARGS__)

#else
#define LOG_INFO(...)
#define LOG_WARN(...)
#define LOG_ERROR(...)
#endif  // CFG_DEBUG_LOG_EN == 1

/*** DEBUG BLOCK ***********************************************************/

#if CFG_BUILD_TYPE == CFG_BUILD_RELEASE
#define DEBUG_BLOCK(...)
#else
#define DEBUG_BLOCK(...)                                                                           \
    do {                                                                                           \
        __VA_ARGS__;                                                                               \
    } while (0)
#endif

/*** STATIC ASSERT *********************************************************/

#ifdef __cplusplus
#ifndef _Static_assert
#define _Static_assert static_assert
#endif
#endif

/** @brief Static assert */
#define STATIC_ASSERT(expression) _Static_assert((expression), #expression)

/*** ASSERT ****************************************************************/

#if CFG_ASSERT_EN == 0

/** @brief Loop that device enters after failed assertion */
void _dbg_panic_loop(const char *format, ...) NORETURN;

/** @brief Set debug breakpoint */
void _dbg_break();

/** @brief Reset device - used in ASSERT only */
void _dbg_reset();

#if CFG_BUILD_TYPE == CFG_BUILD_RELEASE
#define __DEBUG_INTERNAL_BREAK_OR_RESET _dbg_reset
#else
#define __DEBUG_INTERNAL_BREAK_OR_RESET _dbg_break
#endif

// Can be used to break infinite loop
#ifndef CFG_ASSERT_INFINITE_LOOP_EN
#define CFG_ASSERT_INFINITE_LOOP_EN 1
#endif

#define ASSERT_MSG(_X, _Y)                                                                         \
    do {                                                                                           \
        if (!(_X)) {                                                                               \
            HAL_GPIO_WritePin(GPIOF, GPIO_PIN_15, GPIO_PIN_RESET);                                 \
            LOG_ERROR("ASSERT FAILED\n");                                                          \
            LOG_ERROR("  Called from: %s:%d\n", __FILE__, __LINE__);                               \
            LOG_ERROR("  Function:    %s\n", __FUNCTION__);                                        \
            LOG_ERROR("Message:     %s\n", _Y);                                                    \
            __DEBUG_INTERNAL_BREAK_OR_RESET();                                                     \
            _dbg_panic_loop(                                                                       \
                "ASSERT FAILED\nCalled from: %s:%d\nFunction:    %s\nMessage:     %s\n", __FILE__, \
                __LINE__, __FUNCTION__, (_Y));                                                     \
        }                                                                                          \
    } while (0)

#define ASSERT(_X) ASSERT_MSG(_X, #_X)

#else
#define ASSERT(...)
#define ASSERT_MSG(_X, ...)
#endif
