/*****************************************************************************
 * @file debug.c
 *
 * @brief Source file of the debug implementation - redirection to printf
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>

#include "driver/mcuDriver/mcuDriver.h"
#include "driver/swo/swo.h"
#include "driver/uartDriver/uartDriver.h"

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/

void _dbg_swo_printf(uint32_t channel, const char *format, ...)
{
    if (McuDriverIsInInterrupt()) {
        return;
    }

    va_list args;
    va_start(args, format);
    ITM_vWrite(channel, format, args);
    va_end(args);
}

void _dbg_usart_printf(const char *format, ...)
{
    if (McuDriverIsInInterrupt()) {
        return;
    }

    va_list args;
    va_start(args, format);
    uartDriverPrintLog(format, args);
    va_end(args);
}


void _dbg_break()
{
    McuDriverInsertBreakpoint();
}

void _dbg_reset()
{
    // TODO: change build type to CFG_BUILD_TYPE_DEBUG
    // McuDriverSystemReset();
    McuDriverInsertBreakpoint();
}

void _dbg_panic_loop(const char *format, ...)
{
    uint16_t printDelayCount = 0;

    __disable_irq();

    while (1) {
        HAL_Delay(500);

        printDelayCount++;

        HAL_Delay(500);

        // 20 = 10(sec)  because of DelayUs for 500(ms)
        if ((!McuDriverIsInInterrupt()) && (printDelayCount > 10)) {
            va_list args;
            va_start(args, format);
            ITM_vWrite(0, format, args);
            va_end(args);

            printDelayCount = 0;
        }
    }
}
